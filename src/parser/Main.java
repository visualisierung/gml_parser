package parser;

import java.io.*;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

/**
 * Created by David on 09.05.2017.
 */
public class Main {

    static ArrayList<String> airport_name;
    static ArrayList<String> country_name;
    static ArrayList<String> targetAirportID;
    static ArrayList<String> targetAirportLabel;
    static ArrayList<String> targetAirportSource;
    static ArrayList<String> targetAirportValue;
    static ArrayList<Integer> airport_longitude, airport_latitude;
    static ArrayList<Node> nodes;
    static ArrayList<Edge> edges;
    static TreeMap<String, Integer> map;

    static String config = "all";
    static String flightConfig = "";
    static int directedConfig = 0;

    public static void main(String[] args) {
        System.out.println(
                "First:\tenter the desired configuration for the file creation process (all, routes)" +
                        "\nSecond:\tenter desired flight connection configuration (all, exclude)" +
                        "\nThird: \tenter graph configuration" +
                        "\n\nConfiguration:" +
                        "\nall =\tcreates routes file (nodes : airports, edges : connections) + one file for each country" +
                        "\nroutes =\tcreates the routes file only" +
                        "\nall =\tinclude intercontinental flights" +
                        "\nonly =\texclude intercontinental flights" +
                        "\n1 = \tdirected graph" +
                        "\n0 = \tundirected graph");


        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Enter desired configuration: ");
        try {
            config = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print("Enter desired flight connection configuration: ");
        try {
            flightConfig = br.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.print("Enter desired graph configuration: ");
        try {
            try {
                directedConfig = Integer.parseInt(br.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (NumberFormatException nfe) {
            System.err.println("Invalid Format!");
        }


        if (config.equals("all")) {
            importAirLine();
            importRoutes();
            writeGraphToGML();
            getCountryNameList();
            writeCountryNameFile();
            writeCountryRelatedToGML();
        } else if (config.equals("routes")) {
            importAirLine();
            importRoutes();
            writeGraphToGML();
        }


    }


    private static void importAirLine(){
        nodes = new ArrayList<>();
        System.out.println("Import: -> Airports.csv");
        int lineCount = 0;
        try {
            FileReader reader = new FileReader("airports.csv");
            try {
                BufferedReader buffer = new BufferedReader(reader);
                String line;
                while((line = buffer.readLine()) != null){
                    String[] buffered = line.split(";");
                    Node node = new Node();
                    String id = buffered[0];
                    String label = buffered[4];
                    String source = buffered[3];
                    String value = buffered[1];
                    if (label.length() > 2) {
                        node.setId(id);
                        node.setSource(source);
                        node.setLabel(label);
                        node.setValue(value);
                        nodes.add(node);
                    }
                }
                System.out.println("Number of nodes: " + nodes.size());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        finally {

        }

    }


    private static void importRoutes(){
        System.out.println("Import: -> routes.dat");
        edges = new ArrayList<>();
        int lineCount = 0;
        try {
            FileReader reader = new FileReader("routes.csv");
            try {
                BufferedReader buffer = new BufferedReader(reader);
                String line;
                while((line = buffer.readLine()) != null){
                    String[] buffered = line.split(";");
                    Edge edge = new Edge();
                    String label = buffered[1];
                    String source = buffered[3];
                    String target = buffered[5];
                    String sourceName = buffered[2];
                    String targetName = buffered[4];
                    boolean doEntry = false;
                    if (!source.contains("N")) {
                        doEntry = true;
                    } else doEntry = !target.contains("N");
                    if (doEntry) {
                        edge.setLable(label);
                        edge.setSource(source);
                        edge.setTarget(target);
                        edge.setSourceName(sourceName);
                        edge.setTargetName(targetName);
                        edges.add(edge);
                    }

                }
                System.out.println("Number of edges: " + edges.size());
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

    }

    private static void getCountryNameList() {
        map = new TreeMap<>();
        ArrayList<String> set = new ArrayList<>();
        for (int i = 0; i < nodes.size(); i++) {
            map.put(nodes.get(i).getSource(), map.containsKey(nodes.get(i).getSource()) ? map.get(nodes.get(i).getSource()) + 1 : 1);
        }
        for (String k : map.keySet()) {
            System.out.println("" + k + ": " + map.get(k) + " times");
        }

    }

    private static void writeCountryNameFile() {
        System.out.println("writing country name file...");
        FileWriter file = null;
        try {
            file = new FileWriter("country.dat");
            for (String k : map.keySet()) {

                file.write(k + "\n");

            }
            file.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    private static void writeCountryRelatedToGML() {

        int fileCounter = 1;
        for (String k : map.keySet()) {

            ArrayList<String> airportNames = new ArrayList<>();
            String country = k;
            for (int i = 0; i < nodes.size(); i++) {

                if (nodes.get(i).getSource().equals(country)) {
                    airportNames.add(nodes.get(i).getLabel());

                }
            }

            k = k.replace(" ", "");
            FileWriter file = null;
            try {
                try {


                    file = new FileWriter(k + "_" + flightConfig + ".gml");
                    String node = "  node\n";
                    String edge = "  edge\n";
                    String graph = "graph\n";
                    String startTag = "[\n";
                    String closure = "  ]\n";
                    String finalCloseTag = "]";
                    String dir = "  directed " + directedConfig + "\n";
                    String openTag = "  [\n";
                    Date date = new Date();
                    file.write("Creator: David Leisten on " + date.getTime() + "\n");
                    file.write(graph);
                    file.write(startTag);
                    file.write(dir);


                    for (int i = 0; i < nodes.size(); i++) {
                        if (flightConfig.equals("all")) {
                            if (airportNames.contains(nodes.get(i).getLabel())) {
                                String sourceAirportLabel = nodes.get(i).getLabel();
                                getTargetAirportData(sourceAirportLabel);
                                int relatedTargets = targetAirportID.size();
                                if (relatedTargets < 1) {
                                    relatedTargets = 0;
                                }
                                //System.out.println(targetAirportID.size() + " " + targetAirportLabel.size() + " " + targetAirportSource.size());
                                file.write(node);
                                file.write(openTag);
                                file.write("    id " + nodes.get(i).getId() + "\n");
                                file.write("    label " + "\"" + nodes.get(i).getLabel() + "\"" + "\n");
                                file.write("    value " + relatedTargets + "\n");
                                file.write("    source " + "\"" + nodes.get(i).getSource() + "\"" + "\n");
                                file.write(closure);
                                //if(targetAirportID.size() > 5) {

                                for (int l = 0; l < targetAirportID.size(); l++) {
                                    file.write(node);
                                    file.write(openTag);
                                    file.write("    id " + targetAirportID.get(l) + "\n");
                                    file.write("    label " + "\"" + targetAirportValue.get(l) + " (" + targetAirportLabel.get(l) + ")" + "\"" + "\n");
                                    file.write("    source " + "\"" + targetAirportSource.get(l) + "\"" + "\n");
                                    file.write(closure);
                                }
                                //}
                            }
                        } else if (flightConfig.equals("only")) {
                            if (airportNames.contains(nodes.get(i).getLabel())) {
                                String sourceAirportLabel = nodes.get(i).getLabel();
                                getTargetAirportData(sourceAirportLabel);
                                //if(targetAirportID.size()>5) {
                                file.write(node);
                                file.write(openTag);
                                file.write("    id " + nodes.get(i).getId() + "\n");
                                file.write("    label " + "\"" + nodes.get(i).getValue() + " (" + nodes.get(i).getLabel() + ")" + "\"" + "\n");
                                file.write("    source " + "\"" + nodes.get(i).getSource() + "\"" + "\n");
                                file.write(closure);
                                //}
                            }
                        }
                    }
                    for (int i = 0; i < edges.size(); i++) {
                        if (flightConfig.equals("all")) {
                            if (airportNames.contains(edges.get(i).getSourceName())) {
                                file.write(edge);
                                file.write(openTag);
                                file.write("    source " + edges.get(i).getSource() + "\n");
                                file.write("    target " + edges.get(i).getTarget() + "\n");
                                file.write(closure);
                            }
                        } else if (flightConfig.equals("only"))
                            if (airportNames.contains(edges.get(i).getSourceName()) && airportNames.contains(edges.get(i).getDestinationName())) {
                                file.write(edge);
                                file.write(openTag);
                                file.write("    source " + edges.get(i).getSource() + "\n");
                                file.write("    target " + edges.get(i).getTarget() + "\n");
                                file.write(closure);
                            }
                    }
                    file.write(finalCloseTag);

                } finally {
                    file.close();
                    System.out.println("Writing file(" + fileCounter + " of " + map.size() + ")\tfile name: " + k + "_" + flightConfig + ".gml");
                    fileCounter++;

                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }
    }

    private static void writeGraphToGML() {
        FileWriter file = null;
        try {
            try {
                file = new FileWriter("routes.gml");
                String node = "  node\n";
                String edge = "  edge\n";
                String graph = "graph\n";
                String startTag = "[\n";
                String closure = "  ]\n";
                String finalCloseTag = "]";
                String dir = "  directed " + directedConfig + "\n";
                String openTag = "  [\n";
                Date date = new Date();
                file.write("Creator: David Leisten on " + date.getTime() + "\n");
                file.write(graph);
                file.write(startTag);
                file.write(dir);
                for (int i = 0; i < nodes.size(); i++) {
                    file.write(node);
                    file.write(openTag);
                    file.write("    id " + nodes.get(i).getId() + "\n");
                    file.write("    label " + "\"" + nodes.get(i).getLabel() + "\"" + "\n");
                    file.write("    source " + "\"" + nodes.get(i).getSource() + "\"" + "\n");
                    file.write(closure);
                }
                for (int i = 0; i < edges.size(); i++) {
                    file.write(edge);
                    file.write(openTag);
                    file.write("    source " + edges.get(i).getSource() + "\n");
                    file.write("    target " + edges.get(i).getTarget() + "\n");
                    file.write(closure);
                }
                file.write(finalCloseTag);
            } finally {
                file.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void getTargetAirportData(String label) {
        targetAirportID = new ArrayList<>();
        targetAirportLabel = new ArrayList<>();
        targetAirportSource = new ArrayList<>();
        targetAirportValue = new ArrayList<>();
        ArrayList<String> targetIds = new ArrayList<>();
        for (int i = 0; i < edges.size(); i++) {
            if (edges.get(i).getSourceName().equals(label)) {
                targetIds.add(edges.get(i).getTarget());
            }
        }
        for (int j = 0; j < nodes.size(); j++) {
            if (targetIds.contains(nodes.get(j).getId())) {
                targetAirportID.add(nodes.get(j).getId());
                targetAirportLabel.add(nodes.get(j).getLabel());
                targetAirportSource.add(nodes.get(j).getSource());
                targetAirportValue.add(nodes.get(j).getValue());
            }
        }
    }
}
