package parser;

/**
 * Created by David on 09.05.2017.
 */
public class Edge {

    String source;
    String target;
    String lable;
    String sourceName;
    String targetName;

    public Edge() {

    }

    public String getDestinationName() {
        return targetName;
    }

    public void setTargetName(String destinationName) {
        this.targetName = destinationName;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getLable() {
        return lable;
    }

    public void setLable(String lable) {
        this.lable = lable;
    }

    public String getSourceName() {
        return sourceName;
    }

    public void setSourceName(String sourceName) {
        this.sourceName = sourceName;
    }
}
